//
//  PopOverViewController.swift
//  GenericCollection_POC
//
//  Created by Rakesh Kumar Biswal on 03/12/19.
//  Copyright © 2019 RAKESH BISWAL. All rights reserved.
//

import UIKit


class PopOverViewController: UIViewController {
    @IBOutlet weak var containerForpay: UIView!
    @IBOutlet weak var containerForReceive: UIView!
    @IBOutlet weak var mainContainerView: UIView!
    var containerTag:Int?
    private let rotateAnimation = AnimationType.rotate(angle: CGFloat.pi/7)
    private let zoomAnimation = AnimationType.zoom(scale: 0.6)
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.checkAction))
        view.addGestureRecognizer(gesture)
        view.isOpaque = false
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        containerForpay.isHidden = true
        containerForReceive.isHidden = true
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        if containerTag == 100 {
            performAnimationForContainer(container:containerForpay)
        }else{
            performAnimationForContainer(container:containerForReceive)
        }
    }
    
    
    private func performAnimationForContainer(container:UIView){
        if  SharedInstance.shared.animationRequired {
            let rotateAnimation = AnimationType.rotate(angle: CGFloat.pi/7)
            UIView.animate(views: container.subviews,
                           animations: [zoomAnimation, rotateAnimation],
                           duration: 0.35)
            container.isHidden = false
            
        }else{
            UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
                container.isHidden = false
            })
        }
    }
    
    
    
    @objc func checkAction(sender : UITapGestureRecognizer) {
        dismiss()
    }
    
    @IBAction func crossButton(_ sender: Any) {
        dismiss()
    }
    
    func  dismiss() {
        if  SharedInstance.shared.animationRequired {
            UIView.animate(views: mainContainerView.subviews, animations: [zoomAnimation, rotateAnimation], reversed: true,
                           initialAlpha: 1.0, finalAlpha: 0.0, completion: {
                            self.dismiss(animated:false, completion:nil)
            })
            
        } else{
            UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
                self.mainContainerView.isHidden = true
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.dismiss(animated:false, completion:nil)
                }
            })
        }
    }
}
