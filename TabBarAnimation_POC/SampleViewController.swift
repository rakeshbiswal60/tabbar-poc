//
//  SampleViewController.swift
//  GenericCollection_POC
//
//  Created by Rakesh Kumar Biswal on 05/12/19.
//  Copyright © 2019 RAKESH BISWAL. All rights reserved.
//

import UIKit

class SampleViewController: UIViewController {
    @IBOutlet weak var animationSwitch: UISwitch!
    @IBOutlet weak var animationTitle: UILabel!{
        didSet{
            animationTitle.text = animationSwitch.isOn ? "Animation On" : "Animation 0ff"
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        animationSwitch.addTarget(self, action:#selector(changeAnimationMode), for:.valueChanged)
    }
    
   @objc func changeAnimationMode(sender:UISwitch){
        SharedInstance.shared.animationRequired = sender.isOn
        animationTitle.text = animationSwitch.isOn ? "Animation On" : "Animation 0ff"
    }

}
