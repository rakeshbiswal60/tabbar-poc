//
//  MainViewController.swift
//  GenericCollection_POC
//
//  Created by Rakesh Kumar Biswal on 03/12/19.
//  Copyright © 2019 RAKESH BISWAL. All rights reserved.
//

import UIKit


class MainViewController: UITabBarController {
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
    }
}




extension  MainViewController:UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        switch viewController.title {
        case "Circular":
            showPopOver(vc:viewController, tag:100)
            return false
        case "smallCircular":
            showPopOver(vc:viewController, tag:200)
            return false
        default:
            return true
        }
    }
    func showPopOver( vc:UIViewController? = UIViewController(),tag:Int) {
        if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopOverViewController") as? PopOverViewController{
            vc.modalPresentationStyle = .overCurrentContext
            vc.containerTag = tag
            present(vc, animated: false, completion: nil)
        }
    }
}
